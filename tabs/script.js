const tabsName = document.querySelectorAll(`.tabs-title`);
const tabsParagraph = document.querySelectorAll(`.paragraph`);

tabsName.forEach(function(elem){
    elem.addEventListener(`click`, function () {
        const currentTab = elem;
        const parId = currentTab.getAttribute(`data-tab`);
        const currentParagraph = document.querySelector(parId);

        tabsName.forEach(function (elem) {
            elem.classList.remove(`active`)
        });

        tabsParagraph.forEach(function (elem) {
            elem.classList.remove('active-par')
        });

        currentTab.classList.add(`active`);
        currentParagraph.classList.add(`active-par`)
    })
})